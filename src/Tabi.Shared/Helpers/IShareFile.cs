﻿namespace Tabi
{
    public interface IShareFile
    {
        void ShareFile(string path, string mime = "text/plain");
    }
}
