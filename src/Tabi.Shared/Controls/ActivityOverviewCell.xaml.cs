﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabi
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivityOverviewCell : ViewCell
    {
        public ActivityOverviewCell()
        {
            InitializeComponent();
        }
    }
}
