﻿namespace Tabi.Shared.Controls
{
    public interface IMapControl
    {
        void Draw();
        void Clear();
    }
}
