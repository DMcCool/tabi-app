﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabi
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DateSwitchView : ContentView
    {
        public DateSwitchView()
        {
            InitializeComponent();
        }
    }
}
