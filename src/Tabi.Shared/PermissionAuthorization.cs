﻿using System;
namespace Tabi.Shared
{
    public enum PermissionAuthorization
    {
        CheckNotAvailable,
        Restricted,
        NotDetermined,
        Denied,
        Authorized
    }
}
