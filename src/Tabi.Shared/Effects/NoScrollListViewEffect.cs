﻿using Xamarin.Forms;

namespace Tabi
{
    public class NoScrollListViewEffect : RoutingEffect
    {
        public NoScrollListViewEffect() : base("Tabi.NoScrollListViewEffect")
        {
        }
    }
}
