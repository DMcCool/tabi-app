﻿using Xamarin.Forms;

namespace Tabi
{
    public class NoSelectionListViewEffect : RoutingEffect
    {
        public NoSelectionListViewEffect() : base("Tabi.NoSelectionListViewEffect")
        {
        }
    }
}
