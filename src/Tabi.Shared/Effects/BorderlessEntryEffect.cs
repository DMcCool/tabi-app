﻿using Xamarin.Forms;

namespace Tabi
{
    public class BorderlessEntryEffect : RoutingEffect
    {
        public BorderlessEntryEffect() : base("Tabi.BorderlessEntryEffect")
        {
        }
    }
}
