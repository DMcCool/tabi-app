﻿using Xamarin.Forms;

namespace Tabi
{
    public class NoDividerListViewEffect : RoutingEffect
    {
        public NoDividerListViewEffect() : base("Tabi.NoDividerListViewEffect")
        {
        }
    }
}
