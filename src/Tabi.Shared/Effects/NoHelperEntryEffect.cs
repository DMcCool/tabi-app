﻿using Xamarin.Forms;

namespace Tabi
{
    public class NoHelperEntryEffect : RoutingEffect
    {
        public NoHelperEntryEffect() : base("Tabi.NoHelperEntryEffect")
        {
        }
    }
}
