﻿using Xamarin.Forms;

namespace Tabi
{
    public class SystemIconTabEffect : RoutingEffect
    {
        public SystemIconTabEffect() : base("Tabi.SystemIconTabEffect")
        {
        }
    }
}
