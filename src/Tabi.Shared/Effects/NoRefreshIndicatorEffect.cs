﻿using System;
using Xamarin.Forms;

namespace Tabi
{
    public class NoRefreshIndicatorEffect : RoutingEffect
    {
        public NoRefreshIndicatorEffect() : base("Tabi.NoRefreshIndicatorEffect")
        {
        }
    }
}
